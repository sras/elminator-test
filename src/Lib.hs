{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}

module Lib where

import qualified Data.Aeson as A
import Data.Proxy
import qualified Data.Text as T
import Elminator
import GHC.Generics
import OptionsIndex

data Empty a b
  deriving (Generic)

instance (ToHType a, ToHType b) => ToHType (Empty a b)

data Empty2 a b
  deriving (Generic)

instance (ToHType a, ToHType b) => ToHType (Empty2 a b)

data SingleCon =
  SingleCon (Maybe Int) String
  deriving (Generic, ToHType)

instance A.ToJSON SingleCon where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON SingleCon where
  parseJSON = A.genericParseJSON myDefaultOptions

data SingleRecCon =
  SingleRecCon
    { srcF1 :: Int
    , srcF2 :: String
    }
  deriving (Generic, ToHType)

instance A.ToJSON SingleRecCon where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON SingleRecCon where
  parseJSON = A.genericParseJSON myDefaultOptions

data SingleConOneField =
  SingleConOneField Int
  deriving (Generic, ToHType)

instance A.ToJSON SingleConOneField where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON SingleConOneField where
  parseJSON = A.genericParseJSON myDefaultOptions

data SingleRecConOneField =
  SingleRecConOneField
    { srcofF1 :: Int
    }
  deriving (Generic, ToHType)

instance A.ToJSON SingleRecConOneField where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON SingleRecConOneField where
  parseJSON = A.genericParseJSON myDefaultOptions

data TwoCons
  = TCCon1 Int
  | TCCon2 String
  deriving (Generic, ToHType)

instance A.ToJSON TwoCons where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON TwoCons where
  parseJSON = A.genericParseJSON myDefaultOptions

data TwoRecCons
  = RTCCon1
      { rtcF1 :: Int
      }
  | RTCCon2
      { rtcF2 :: String
      }
  deriving (Generic, ToHType)

instance A.ToJSON TwoRecCons where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON TwoRecCons where
  parseJSON = A.genericParseJSON myDefaultOptions

data BigCon =
  BigCon
    Int
    Int
    String
    String
    Float
    Int
    Int
    Int
    Float
    String
    String
    Float
    Int
    TwoCons
    SingleRecCon
  deriving (Generic, ToHType)

instance A.ToJSON BigCon where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON BigCon where
  parseJSON = A.genericParseJSON myDefaultOptions

data Comment =
  Comment
    { cContent :: String
    , cReplies :: Maybe Comment
    }
  deriving (Generic, ToHType)

instance A.ToJSON Comment where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON Comment where
  parseJSON = A.genericParseJSON myDefaultOptions

data BigRecCon =
  BigRecCon
    { bcF1 :: Int
    , bcF2 :: Int
    , bcF3 :: String
    , bcF4 :: String
    , bcF5 :: Float
    , bcF6 :: Int
    , bcF7 :: Int
    , bcF8 :: Int
    , bcF9 :: Float
    , bcF10 :: String
    , bcF11 :: String
    , bcF12 :: Float
    , bcF13 :: Int
    , bcF14 :: TwoCons
    , bcF15 :: SingleRecCon
    }
  deriving (Generic, ToHType)

instance A.ToJSON BigRecCon where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON BigRecCon where
  parseJSON = A.genericParseJSON myDefaultOptions

data MixedCons
  = MxCon1
      { mxcF1 :: Int
      }
  | MxCon2 String
  deriving (Generic, ToHType)

instance A.ToJSON MixedCons where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON MixedCons where
  parseJSON = A.genericParseJSON myDefaultOptions

data WithSimpleMaybes
  = WithSimpleMaybesC1 (Maybe Int) (Maybe String)
  | WithSimpleMaybesC2 (Maybe String) (Maybe Int)
  deriving (Generic, ToHType)

instance A.ToJSON WithSimpleMaybes where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON WithSimpleMaybes where
  parseJSON = A.genericParseJSON myDefaultOptions

data WithMaybes =
  WithMaybes
    { mbF1 :: Maybe Int
    , mbF2 :: Maybe String
    }
  deriving (Generic, ToHType)

instance A.ToJSON WithMaybes where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON WithMaybes where
  parseJSON = A.genericParseJSON myDefaultOptions

data WithMaybesPoly a b =
  WithMaybesPoly
    { mbpF1 :: Maybe a
    , mbpF2 :: Maybe b
    }
  deriving (Generic, ToHType)

instance A.ToJSON (WithMaybesPoly String Float) where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON (WithMaybesPoly String Float) where
  parseJSON = A.genericParseJSON myDefaultOptions

data Phantom a =
  Phantom String
  deriving (Generic, ToHType)

instance A.ToJSON (Phantom a) where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON (Phantom a) where
  parseJSON = A.genericParseJSON myDefaultOptions

data TypeWithPhantom a =
  TypeWithPhantom (Phantom a)
  deriving (Generic, ToHType)

data RecWithList =
  RecWithList
    { rwList :: [String]
    , rwOther :: Float
    }
  deriving (Generic, ToHType)

instance A.ToJSON RecWithList where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON RecWithList where
  parseJSON = A.genericParseJSON myDefaultOptions

data IndRecStart =
  IndRecStart (Maybe IndRec2) Int
  deriving (Generic, ToHType, Show)

instance A.ToJSON IndRecStart where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON IndRecStart where
  parseJSON = A.genericParseJSON myDefaultOptions

data IndRec2 =
  IndRec2 IndRec3
  deriving (Generic, ToHType, Show)

instance A.ToJSON IndRec2 where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON IndRec2 where
  parseJSON = A.genericParseJSON myDefaultOptions

data IndRec3 =
  IndRec3 IndRecStart
  deriving (Generic, ToHType, Show)

instance A.ToJSON IndRec3 where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON IndRec3 where
  parseJSON = A.genericParseJSON myDefaultOptions

newtype NTSingleCon =
  NTSingleCon
    { ntField :: Int
    }
  deriving (Generic, ToHType)

instance A.ToJSON NTSingleCon where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON NTSingleCon where
  parseJSON = A.genericParseJSON myDefaultOptions

newtype NTSingleCon2 =
  NTSingleCon2
    { ntField2 :: Int
    }
  deriving (Generic, ToHType)

instance A.ToJSON NTSingleCon2 where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON NTSingleCon2 where
  parseJSON = A.genericParseJSON myDefaultOptions

newtype Tuples =
  Tuples
    { tuples :: (Int, String, Float)
    }
  deriving (Generic, ToHType)

instance A.ToJSON Tuples where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON Tuples where
  parseJSON = A.genericParseJSON myDefaultOptions

newtype NestedTuples =
  NestedTuples
    { nsttuples :: (Int, (String, Float))
    }
  deriving (Generic, ToHType)

newtype NestedTuplesPoly a =
  NestedTuplesPoly
    { nsttuplespoly :: (Int, (String, a))
    }
  deriving (Generic, ToHType)

instance A.ToJSON NestedTuples where
  toJSON = A.genericToJSON myDefaultOptions

instance A.FromJSON NestedTuples where
  parseJSON = A.genericParseJSON myDefaultOptions

data MyExtType a b =
  MyExtType
    { extTypeField1 :: a
    , extTypeField2 :: b
    }
  deriving (Generic)

instance (A.ToJSON a, A.ToJSON b) => A.ToJSON (MyExtType a b) where
  toJSON = A.genericToJSON myDefaultOptions

instance (A.FromJSON a, A.FromJSON b) => A.FromJSON (MyExtType a b) where
  parseJSON = A.genericParseJSON myDefaultOptions

instance (ToHType a, ToHType b) => ToHType (MyExtType a b) where
  toHType _ = do
    ha <- toHType (Proxy :: Proxy a)
    hb <- toHType (Proxy :: Proxy b)
    pure $
      HExternal
        (ExInfo
           ("External", "MyExtType")
           (Just ("External", "encodeMyExtType"))
           (Just ("External", "decodeMyExtType"))
           [ha, hb])

newtype TypeWithExt a =
  TypeWithExt
    { tweField1 :: MyExtType (Maybe a) Int
    }
  deriving (Generic, ToHType)

instance (A.ToJSON a) => A.ToJSON (TypeWithExt a) where
  toJSON = A.genericToJSON myDefaultOptions

instance (A.FromJSON a) => A.FromJSON (TypeWithExt a) where
  parseJSON = A.genericParseJSON myDefaultOptions

newtype WithEmptyTuple a =
  WithEmptyTuple a
  deriving (Generic, ToHType)

instance (A.ToJSON a) => A.ToJSON (WithEmptyTuple a) where
  toJSON = A.genericToJSON myDefaultOptions

instance (A.FromJSON a) => A.FromJSON (WithEmptyTuple a) where
  parseJSON = A.genericParseJSON myDefaultOptions

data Phantom2 a =
  Phantom2
  deriving (Generic, ToHType)

newtype PhantomWrapper =
  PhantomWrapper
    { field1 :: Phantom2 PhantomWrapper
    }
  deriving (Generic, ToHType)

newtype TextWraper =
  TextWraper
    { txWrapper :: T.Text
    }
  deriving (Generic, ToHType)

data IndRecPolyStart a =
  IndRecPolyStart (Phantom (IndRecPoly2 a)) Int
  deriving (Generic, ToHType)

data IndRecPoly2 a =
  IndRecPoly2 (IndRecPoly3 a)
  deriving (Generic, ToHType)

data IndRecPoly3 a =
  IndRecPoly3 (IndRecPolyStart a)
  deriving (Generic, ToHType)

optionsList :: [A.Options]
optionsList = do
  flm <- [(\x -> "fm" ++ x), id]
  ctm <- [(\x -> "fm" ++ x), id]
  antst <- [True, False]
  onf <- [True, False]
  se <- seList
  uur <- [True, False]
  tsc <- [True, False]
  pure $
    A.defaultOptions
      { A.fieldLabelModifier = flm
      , A.constructorTagModifier = ctm
      , A.allNullaryToStringTag = antst
      , A.omitNothingFields = onf
      , A.sumEncoding = se
      , A.unwrapUnaryRecords = uur
      , A.tagSingleConstructors = tsc
      }

seList :: [A.SumEncoding]
seList =
  let taggedObjects = do
        tfn <- ["tag", "myTag"]
        cfn <- ["contents", "myContents"]
        pure $ A.TaggedObject tfn cfn
   in taggedObjects ++
      [A.UntaggedValue, A.ObjectWithSingleField, A.TwoElemArray]

myDefaultOptions :: A.Options
myDefaultOptions = optionsList !! idx

builder :: Builder
builder = do
  include (Proxy :: Proxy SingleCon) $ Everything Mono
  include (Proxy :: Proxy SingleRecCon) $ Everything Mono
  include (Proxy :: Proxy SingleConOneField) $ Everything Mono
  include (Proxy :: Proxy SingleRecConOneField) $ Everything Mono
  include (Proxy :: Proxy TwoCons) $ Everything Mono
  include (Proxy :: Proxy TwoRecCons) $ Everything Mono
  include (Proxy :: Proxy BigCon) $ Everything Mono
  include (Proxy :: Proxy BigRecCon) $ Everything Mono
  include (Proxy :: Proxy MixedCons) $ Everything Mono
  include (Proxy :: Proxy Comment) $ Everything Mono
  include (Proxy :: Proxy WithMaybes) $ Everything Mono
  include (Proxy :: Proxy WithSimpleMaybes) $ Everything Mono
  include (Proxy :: Proxy (WithMaybesPoly (Maybe String) Float)) $
    Definiton Poly
  include (Proxy :: Proxy (WithMaybesPoly (Maybe String) Float)) EncoderDecoder
  include (Proxy :: Proxy (Phantom ())) $ Everything Poly
  include (Proxy :: Proxy (TypeWithPhantom Float)) $ Everything Poly
  include (Proxy :: Proxy RecWithList) $ Everything Mono
  include (Proxy :: Proxy IndRecStart) $ Everything Mono
  include (Proxy :: Proxy IndRec2) $ Everything Mono
  include (Proxy :: Proxy IndRec3) $ Everything Mono
  include (Proxy :: Proxy NTSingleCon) $ Everything Mono
  include (Proxy :: Proxy NTSingleCon2) $ Everything Poly
  include (Proxy :: Proxy Tuples) $ Everything Mono
  include (Proxy :: Proxy NestedTuples) $ Everything Mono
  include (Proxy :: Proxy (NestedTuplesPoly ())) $ Definiton Poly
  include (Proxy :: Proxy (TypeWithExt ())) $ Everything Poly
  include (Proxy :: Proxy (WithEmptyTuple ())) $ Everything Poly
  include (Proxy :: Proxy (Phantom2 ())) $ Everything Poly
  include (Proxy :: Proxy TextWraper) $ Everything Poly
  include (Proxy :: Proxy PhantomWrapper) $ Everything Poly
  include (Proxy :: Proxy (IndRecPolyStart ())) $ Everything Poly
  include (Proxy :: Proxy (IndRecPoly2 ())) $ Everything Poly
  include (Proxy :: Proxy (IndRecPoly3 ())) $ Everything Poly
