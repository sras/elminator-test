{-# Language OverloadedStrings #-}
{-# Language TypeOperators #-}
{-# Language FlexibleInstances #-}
{-# Language FlexibleContexts #-}
{-# Language DataKinds #-}
{-# Language DeriveGeneric #-}
{-# Language MultiParamTypeClasses #-}
{-# Language TypeFamilies #-}
{-# Language TemplateHaskell #-}

module Server where


import Servant ( Post
               , JSON
               , ServerT
               , hoistServer
               , ReqBody
               , Proxy(..)
               , type (:>)      -- Syntax for importing type operator
               , type (:<|>)
               , (:<|>)(..)
               )
import Servant.Server (Handler, Application, serve)
import Network.Wai.Handler.Warp (run)
import Control.Monad.IO.Class (liftIO)
import Network.Wai.Middleware.Static (static)
import Control.Monad.Reader
import System.Log.FastLogger

import OptionsIndex
import Lib
import CodeGen
import Data.Text as T

type MyAppType = ReaderT (FastLogger, IO ()) IO
--
type AppType
   =  "SingleCon" :> (ReqBody '[JSON] SingleCon) :> (Post '[JSON] SingleCon)
  :<|> "SingleRecCon" :> (ReqBody '[JSON] SingleRecCon) :> (Post '[JSON] SingleRecCon)
  :<|> "SingleConOneField" :> (ReqBody '[JSON] SingleConOneField) :> (Post '[JSON] SingleConOneField)
  :<|> "SingleRecConOneField" :> (ReqBody '[JSON] SingleRecConOneField) :> (Post '[JSON] SingleRecConOneField)
  :<|> "TwoCons" :> (ReqBody '[JSON] TwoCons) :> (Post '[JSON] TwoCons)
  :<|> "TwoRecCons" :> (ReqBody '[JSON] TwoRecCons) :> (Post '[JSON] TwoRecCons)
  :<|> "BigCon" :> (ReqBody '[JSON] BigCon) :> (Post '[JSON] BigCon)
  :<|> "BigRecCon" :> (ReqBody '[JSON] BigRecCon) :> (Post '[JSON] BigRecCon)
  :<|> "MixedCons" :> (ReqBody '[JSON] MixedCons) :> (Post '[JSON] MixedCons)
  :<|> "Comment" :> (ReqBody '[JSON] Comment) :> (Post '[JSON] Comment)
  :<|> "WithMaybes" :> (ReqBody '[JSON] WithMaybes) :> (Post '[JSON] WithMaybes)
  :<|> "WithSimpleMaybes" :> (ReqBody '[JSON] WithSimpleMaybes) :> (Post '[JSON] WithSimpleMaybes)
  :<|> "WithMaybesPoly" :> (ReqBody '[JSON] (WithMaybesPoly String Float)) :> (Post '[JSON] (WithMaybesPoly String Float))
  :<|> "Phantom" :> (ReqBody '[JSON] (Phantom Int)) :> (Post '[JSON] (Phantom Int))
  :<|> "RecWithList" :> (ReqBody '[JSON] RecWithList) :> (Post '[JSON] RecWithList)
  :<|> "IndRecStart" :> (ReqBody '[JSON] IndRecStart) :> (Post '[JSON] IndRecStart)
  :<|> "NTSingleCon" :> (ReqBody '[JSON] NTSingleCon) :> (Post '[JSON] NTSingleCon)
  :<|> "NTSingleCon2" :> (ReqBody '[JSON] NTSingleCon2) :> (Post '[JSON] NTSingleCon2)
  :<|> "Tuples" :> (ReqBody '[JSON] Tuples) :> (Post '[JSON] Tuples)
  :<|> "NestedTuples" :> (ReqBody '[JSON] NestedTuples) :> (Post '[JSON] NestedTuples)
  :<|> "TypeWithExt" :> (ReqBody '[JSON] (TypeWithExt ())) :> (Post '[JSON] (TypeWithExt ()))
  :<|> "WithEmptyTuple" :> (ReqBody '[JSON] (WithEmptyTuple ())) :> (Post '[JSON] (WithEmptyTuple ()))
  :<|> "error" :> ReqBody '[JSON] String :> Post '[JSON] String
  :<|> "success" :> ReqBody '[JSON] String :> Post '[JSON] String

appHandlers :: ServerT AppType MyAppType
appHandlers
  =  pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> pure
 :<|> writeError
 :<|> writeSuccess

writeError :: String -> MyAppType String
writeError s = do
  (logger, _) <- ask
  liftIO $ do
    logger $ toLogStr' ("Error:" ++ idxString ++ ":" ++ s ++ "\n")
    pure s

writeSuccess :: String -> MyAppType String
writeSuccess s = do
  (logger, _) <- ask
  liftIO $ do
    logger $ toLogStr' ("Success:"++ idxString ++ ":" ++ s ++ "\n")
    pure s

toLogStr' :: String -> LogStr
toLogStr' = toLogStr

idxString :: String
idxString = show idx

handlerServer :: (FastLogger, IO ()) -> ServerT AppType Handler
handlerServer fl = hoistServer (Proxy :: Proxy AppType) readerToHandler appHandlers
  where
    readerToHandler :: MyAppType x -> Handler x
    readerToHandler r = liftIO $ runReaderT r fl

app :: (FastLogger, IO ()) -> Application
app fl = serve (Proxy :: Proxy AppType) $ handlerServer fl

server :: IO ()
server = do
  putStrLn $ "Testing Aeson options index : " ++ (show idx)
  putStrLn $ "Generated elm source size: " ++ (show $ T.length elmSource)
  fl@(f, _) <- newFastLogger (LogFileNoRotate "test-log.txt" 512)
  f $ toLogStr ("\n----------------------\n"::String)
  f $ toLogStr ("Starting "++ show idx ++ "...\n")
  f $ toLogStr' $ show myDefaultOptions
  f $ toLogStr ("\n"::String)
  run 4000 $ static $ app fl
