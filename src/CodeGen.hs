{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module CodeGen where

import Data.Text
import Elminator

import Lib

elmSource :: Text
elmSource =
  $(generateFor
      Elm0p19
      myDefaultOptions
      "Autogen"
      (Just "./elm-app/src/Autogen.elm")
      builder)
