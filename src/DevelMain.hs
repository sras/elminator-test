module DevelMain where

import Data.Text as T
import Lib
import Rapid
import Server

import CodeGen
import Network.Wai.Handler.Warp (run)
import Network.Wai.Middleware.Static (static)
import System.Log.FastLogger

import OptionsIndex

update :: IO ()
update =
  rapid 0 $ \r -> do
    fl@(f, _) <-
      Rapid.createRef r "fast-logger" $
      newFastLogger (LogFileNoRotate "test-log.txt" 512)
    restart r "webserver" $ do
      putStrLn $ "Testing Aeson options index : " ++ (show idx)
      putStrLn $ "Generated elm source size: " ++ (show $ T.length elmSource)
      f $ toLogStr ("\n----------------------\n" :: String)
      f $ toLogStr ("Starting test for Aeson option index: " ++ (show idx))
      f $ toLogStr ("\n" :: String)
      f $ toLogStr' $ show myDefaultOptions
      f $ toLogStr ("\n" :: String)
      run 4000 $ static $ app fl
