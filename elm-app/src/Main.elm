module Main exposing (..)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Autogen
import Http
import Json.Encode as E
import Json.Decode as D
import Debug
import External exposing (..)

type alias Model =
    {
    }

type Msg
    = NotifyError String
    | NotifySuccess String
    | WhatEver (Result Http.Error ())

mkMsg : x -> String -> (Result Http.Error x -> Msg)
mkMsg rv tn  =
  let
    successMsg = tn
    inequalityError = "Got a different value for:" ++ tn
    badbodyError = "Bad status error with:" ++ tn
    someError = "Error with:" ++ tn
    fn r = case r of
        Ok x -> if x == rv then (Debug.log successMsg <| NotifySuccess successMsg) else let (a,_) = Debug.log inequalityError  <|  (NotifyError inequalityError, (rv, x)) in a
        Err x -> case x of 
          Http.BadBody s -> Debug.log badbodyError <| NotifyError badbodyError
          _ -> Debug.log someError  <| NotifyError someError
  in fn

view : Model -> Html Msg
view model = div [] [text "Hi there"]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model = case msg of
  NotifySuccess s ->
    let cmd = Http.post { url = "http://localhost:4000/success", body = Http.jsonBody <| E.string s, expect = Http.expectWhatever WhatEver }
    in (model, cmd)
  NotifyError s ->
    let cmd = Http.post { url = "http://localhost:4000/error", body = Http.jsonBody <| E.string s, expect = Http.expectWhatever WhatEver }
    in (model, cmd)
  _ -> (model, Cmd.none)

myF1 : String -> String
myF1 x = x

myF2 : String -> Int
myF2 _ = 10

myF3 : String -> Int
myF3 = myF1 >> myF2

type alias Ta a = { name : a}


init : Maybe Model -> ( Model, Cmd Msg )
init flags = ({}, Cmd.batch [getSingleCon, getSingleRecCon, getSingleConOneField, getSingleRecConOneField, getTwoCons1, getTwoCons2, getTwoRecCons1, getTwoRecCons2, getBigCon, getBigRecCon, getMixedCons1, getMixedCons2, getComment, getWithMaybes, getWithMaybesWithNothing, getWithSimpleMaybesC1, getWithSimpleMaybesC2, getWithMaybesPoly, getPhantom, getRecWithList, getIndRecStart, getNTSingleCon, getNTSingleCon2, getTuples, getNestedTuples, getWithEmptyTuple])

getWithEmptyTuple : Cmd Msg
getWithEmptyTuple = let jsonb = Autogen.WithEmptyTuple () in Http.post { url = "http://localhost:4000/WithEmptyTuple", body = Http.jsonBody (Autogen.encodeWithEmptyTuple <| jsonb), expect = (Http.expectJson (mkMsg jsonb "WithEmptyTuple") Autogen.decodeWithEmptyTuple) }

getTypeWithExt : Cmd Msg
getTypeWithExt = let jsonb = Autogen.TypeWithExt { tweField1 = MyExtType { extTypeField1 = Just (), extTypeField2 = 10} } in Http.post { url = "http://localhost:4000/TypeWithExt", body = Http.jsonBody (Autogen.encodeTypeWithExt <| jsonb), expect = (Http.expectJson (mkMsg jsonb "TypeWithExt") Autogen.decodeTypeWithExt) }


getTuples : Cmd Msg
getTuples = let jsonb = Autogen.Tuples { tuples = (1, "asd", 4.5)} in Http.post { url = "http://localhost:4000/Tuples", body = Http.jsonBody (Autogen.encodeTuples <| jsonb), expect = (Http.expectJson (mkMsg jsonb "Tuples" ) Autogen.decodeTuples) }

getNestedTuples : Cmd Msg
getNestedTuples = let jsonb = Autogen.NestedTuples { nsttuples = (1, ("asd", 4.5))} in Http.post { url = "http://localhost:4000/NestedTuples", body = Http.jsonBody (Autogen.encodeNestedTuples <| jsonb), expect = (Http.expectJson (mkMsg jsonb "NestedTuples" ) Autogen.decodeNestedTuples) }


getNTSingleCon : Cmd Msg
getNTSingleCon = let jsonb = Autogen.NTSingleCon {ntField = 102} in Http.post { url = "http://localhost:4000/NTSingleCon", body = Http.jsonBody (Autogen.encodeNTSingleCon <| jsonb), expect = (Http.expectJson (mkMsg jsonb "NTSingleCon" ) Autogen.decodeNTSingleCon) }


getNTSingleCon2 : Cmd Msg
getNTSingleCon2 = let jsonb = Autogen.NTSingleCon2 {ntField2 = 223} in Http.post { url = "http://localhost:4000/NTSingleCon2", body = Http.jsonBody (Autogen.encodeNTSingleCon2 <| jsonb), expect = (Http.expectJson (mkMsg jsonb "NTSingleCon2" ) Autogen.decodeNTSingleCon2) }

getIndRecStart : Cmd Msg
getIndRecStart = let jsonb = Autogen.IndRecStart (Just (Autogen.IndRec2 (Autogen.IndRec3 (Autogen.IndRecStart Nothing 20)))) 102 in Http.post { url = "http://localhost:4000/IndRecStart", body = Http.jsonBody (Autogen.encodeIndRecStart <| jsonb), expect = (Http.expectJson (mkMsg jsonb "IndRecStart" ) Autogen.decodeIndRecStart) }

getRecWithList : Cmd Msg
getRecWithList = let jsonb = Autogen.RecWithList { rwList = ["Sras"], rwOther = 2.3 } in Http.post { url = "http://localhost:4000/RecWithList", body = Http.jsonBody (Autogen.encodeRecWithList <| jsonb), expect = (Http.expectJson (mkMsg jsonb "RecWithList" ) Autogen.decodeRecWithList) }

getPhantom : Cmd Msg
getPhantom = let jsonb = Autogen.Phantom "Sras" in Http.post { url = "http://localhost:4000/Phantom", body = Http.jsonBody (Autogen.encodePhantom <| jsonb), expect = (Http.expectJson (mkMsg jsonb "Phantom" ) Autogen.decodePhantom) }


getSingleCon : Cmd Msg
getSingleCon = let jsonb = Autogen.SingleCon (Just 10) "Sras" in Http.post { url = "http://localhost:4000/SingleCon", body = Http.jsonBody (Autogen.encodeSingleCon <| jsonb), expect = (Http.expectJson (mkMsg jsonb "SingleCon" ) Autogen.decodeSingleCon) }


getSingleRecCon : Cmd Msg
getSingleRecCon = let jsonb = Autogen.SingleRecCon { srcF1 = 10, srcF2 = "Sras"} in Http.post { url = "http://localhost:4000/SingleRecCon", body = Http.jsonBody (Autogen.encodeSingleRecCon <| jsonb), expect = (Http.expectJson (mkMsg jsonb "SingleRecCon" ) Autogen.decodeSingleRecCon) }

getSingleConOneField : Cmd Msg
getSingleConOneField = let jsonb = Autogen.SingleConOneField 10 in Http.post { url = "http://localhost:4000/SingleConOneField", body = Http.jsonBody (Autogen.encodeSingleConOneField <| jsonb), expect = (Http.expectJson (mkMsg jsonb "SingleConOneField" ) Autogen.decodeSingleConOneField) }

getSingleRecConOneField : Cmd Msg
getSingleRecConOneField = let jsonb = Autogen.SingleRecConOneField { srcofF1 = 10 } in Http.post { url = "http://localhost:4000/SingleRecConOneField", body = Http.jsonBody (Autogen.encodeSingleRecConOneField <| jsonb), expect = (Http.expectJson (mkMsg jsonb "SingleRecConOneField" ) Autogen.decodeSingleRecConOneField) }

getTwoCons1 : Cmd Msg
getTwoCons1 = let jsonb = Autogen.TCCon1 10 in Http.post { url = "http://localhost:4000/TwoCons", body = Http.jsonBody (Autogen.encodeTwoCons <| jsonb), expect = (Http.expectJson (mkMsg jsonb "TwoCons" ) Autogen.decodeTwoCons) }

getTwoCons2 : Cmd Msg
getTwoCons2 = let jsonb = Autogen.TCCon2 "Sras" in Http.post { url = "http://localhost:4000/TwoCons", body = Http.jsonBody (Autogen.encodeTwoCons <| jsonb), expect = (Http.expectJson (mkMsg jsonb "TwoCons" ) Autogen.decodeTwoCons) }

getTwoRecCons1 : Cmd Msg
getTwoRecCons1 = let jsonb = Autogen.RTCCon1 { rtcF1 = 10 } in Http.post { url = "http://localhost:4000/TwoRecCons", body = Http.jsonBody (Autogen.encodeTwoRecCons <| jsonb), expect = (Http.expectJson (mkMsg jsonb "TwoRecCons" ) Autogen.decodeTwoRecCons) }

getTwoRecCons2 : Cmd Msg
getTwoRecCons2 = let jsonb = Autogen.RTCCon2 { rtcF2 = "Sras" } in Http.post { url = "http://localhost:4000/TwoRecCons", body = Http.jsonBody (Autogen.encodeTwoRecCons <| jsonb), expect = (Http.expectJson (mkMsg jsonb "TwoRecCons" ) Autogen.decodeTwoRecCons) }

getBigCon : Cmd Msg
getBigCon = let jsonb = Autogen.BigCon 12 23 "asdas" "asdasd" 2.3 12 25 54 21.2 "werwr" "asdaSD" 2.3 12 (Autogen.TCCon2 "ssad") (Autogen.SingleRecCon  { srcF1 = 10, srcF2 = "asdasd" }) in Http.post { url = "http://localhost:4000/BigCon", body = Http.jsonBody (Autogen.encodeBigCon <| jsonb), expect = (Http.expectJson (mkMsg jsonb "BigCon" ) Autogen.decodeBigCon) }

getBigRecCon : Cmd Msg
getBigRecCon = let jsonb = Autogen.BigRecCon { bcF1 = 12, bcF2 = 23, bcF3 = "asdas", bcF4 = "asdasd", bcF5 = 2.3, bcF6 = 12, bcF7 = 25, bcF8 = 54, bcF9 = 21.2, bcF10 = "werwr", bcF11 = "asdaSD", bcF12 = 2.3, bcF13 = 12, bcF14 = (Autogen.TCCon2 "ssad"), bcF15 = (Autogen.SingleRecCon  { srcF1 = 10, srcF2 = "asdasd" }) } in Http.post { url = "http://localhost:4000/BigRecCon", body = Http.jsonBody (Autogen.encodeBigRecCon <| jsonb), expect = (Http.expectJson (mkMsg jsonb "BigRecCon" ) Autogen.decodeBigRecCon) }


getMixedCons1 : Cmd Msg
getMixedCons1 = let jsonb = Autogen.MxCon1 { mxcF1 = 10 } in Http.post { url = "http://localhost:4000/MixedCons", body = Http.jsonBody (Autogen.encodeMixedCons <| jsonb), expect = (Http.expectJson (mkMsg jsonb "MixedCons" ) Autogen.decodeMixedCons) }

getMixedCons2 : Cmd Msg
getMixedCons2 = let jsonb = Autogen.MxCon2 "asdad" in Http.post { url = "http://localhost:4000/MixedCons", body = Http.jsonBody (Autogen.encodeMixedCons <| jsonb), expect = (Http.expectJson (mkMsg jsonb "MixedCons" ) Autogen.decodeMixedCons) }


getComment : Cmd Msg
getComment = let jsonb = Autogen.Comment {cContent = "somec", cReplies = Just <| Autogen.Comment {cContent = "somec", cReplies = Nothing }} in Http.post { url = "http://localhost:4000/Comment", body = Http.jsonBody (Autogen.encodeComment <| jsonb), expect = (Http.expectJson (mkMsg jsonb "Comment" ) Autogen.decodeComment) }

getWithMaybes : Cmd Msg
getWithMaybes = let jsonb = Autogen.WithMaybes {mbF1 = Just 10, mbF2 = Just "Sras"} in Http.post { url = "http://localhost:4000/WithMaybes", body = Http.jsonBody (Autogen.encodeWithMaybes <| jsonb), expect = (Http.expectJson (mkMsg jsonb "WithMaybes" ) Autogen.decodeWithMaybes) }

getWithMaybesWithNothing : Cmd Msg
getWithMaybesWithNothing = let jsonb = Autogen.WithMaybes {mbF1 = Just 10, mbF2 = Nothing} in Http.post { url = "http://localhost:4000/WithMaybes", body = Http.jsonBody (Autogen.encodeWithMaybes <| jsonb), expect = (Http.expectJson (mkMsg jsonb "WithMaybes" ) Autogen.decodeWithMaybes) }

getWithSimpleMaybesC1 : Cmd Msg
getWithSimpleMaybesC1 = let jsonb = Autogen.WithSimpleMaybesC1 (Just 10) Nothing in Http.post { url = "http://localhost:4000/WithSimpleMaybes", body = Http.jsonBody (Autogen.encodeWithSimpleMaybes <| jsonb), expect = (Http.expectJson (mkMsg jsonb "WithSimpleMaybes" ) Autogen.decodeWithSimpleMaybes) }

getWithSimpleMaybesC2 : Cmd Msg
getWithSimpleMaybesC2 = let jsonb = Autogen.WithSimpleMaybesC2 Nothing (Just 10) in Http.post { url = "http://localhost:4000/WithSimpleMaybes", body = Http.jsonBody (Autogen.encodeWithSimpleMaybes <| jsonb), expect = (Http.expectJson (mkMsg jsonb "WithSimpleMaybes" ) Autogen.decodeWithSimpleMaybes) }


getWithMaybesPoly : Cmd Msg
getWithMaybesPoly = let jsonb = Autogen.WithMaybesPoly {mbpF1 = Just (Just "sras"), mbpF2 = Just 10} in Http.post { url = "http://localhost:4000/WithMaybesPoly", body = Http.jsonBody (Autogen.encodeWithMaybesPoly <| jsonb), expect = (Http.expectJson (mkMsg jsonb "WithMaybesPoly" ) Autogen.decodeWithMaybesPoly) }

main : Program (Maybe Model) Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , view = \model -> { title = "Elminator Test", body = [view model] }
        , subscriptions = \_ -> Sub.none
        }
