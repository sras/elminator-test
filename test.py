import subprocess
import os
import time
import threading
import socket
import sys
import http.client
from websocket import create_connection
import json

#subprocess.run(["stack", "clean"])

def get_rd_conn():
    while True:
        try:
            hc = http.client.HTTPConnection("localhost:9222")
            hc.request("GET", "/json")
            res = hc.getresponse()
            resp_str = res.read().decode()
            print("Response=")
            print(resp_str)
            print("--------------")
            cn_url = json.loads(resp_str)[0]['webSocketDebuggerUrl']
        except:
            continue
        break
    return create_connection(cn_url)

def sendMsg(msg):
    s = socket.socket()         # Create a socket object
    s.connect(('localhost', 1880))
    s.sendall(msg)
    s.close()

def check_for_errors(olc):
    try:
        lines = list(open('test-log.txt', 'r'))
    except:
        return 0
    lc = 0
    for line in lines:
        lc = lc + 1
        if line.startswith("Error:"):
            print(''.join(lines))
            sys.exit(1)
    print("{} results".format(str(lc - olc)))
    print("No errors in {} lines".format(str(lc)))
    return lc

def wait_for_autogen(fl):
    while True:
        if os.path.isfile(fl):
            return
        else:
            time.sleep(1)

def dump_log():
    with open('test-log.txt', encoding='utf8') as f:
        print(f.read())


nlc = -1
sendMsg(":l src/DevelMain\n".encode())
subprocess.Popen(["chromium","--headless", "--disable-gpu", "--remote-debugging-port=9222"])
os.system("rm test-log.txt")

while True:
    try:
        ws = get_rd_conn()
        break
    except ConnectionRefusedError:
        pass

for i in range (0, 448):
    os.system("sed -i '$d' src/OptionsIndex.hs")
    os.system("echo 'idx={}' >> src/OptionsIndex.hs".format(str(i)))
    os.system("rm elm-app/src/Autogen.elm")
    os.system("rm main.js")
    sendMsg(":reload\n".encode())
    sendMsg("update\n\n".encode())
    wait_for_autogen("elm-app/src/Autogen.elm")
    time.sleep(0.01)
    subprocess.run(["elm", "make", "src/Main.elm", "--output", "../main.js"], cwd="elm-app")
    wait_for_autogen("main.js")
    time.sleep(0.01)
    rs = ws.send(json.dumps({'id': 2, 'method': 'Page.navigate', 'params': {'url': 'http://localhost:4000/index.html'}}))
    print("Waiting for report")
    time.sleep(1)
    nlc = check_for_errors(nlc)

dump_log()
sys.exit(0)
